package g30125.rusu.narcis.l2.e1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class ex1 {
	
	public static void main(String[] args) throws IOException { 
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter Integer 1:");
        int i = Integer.parseInt(br.readLine());
        System.out.print("Enter Integer 2:");
        int j = Integer.parseInt(br.readLine());
        if(i>j)
        {
        	 System.out.print(i);
        }
        else
        {
        	System.out.print(j);
        }
    }
	
}
