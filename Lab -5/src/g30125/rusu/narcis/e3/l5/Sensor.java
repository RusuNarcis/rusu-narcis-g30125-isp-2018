package g30125.rusu.narcis.e3.l5;

abstract  class Sensor {
private String location;
abstract public int readValue();
public String getLocation() {
	return location;
}
}

