package g30125.rusu.narcis.e2.l5;

public class RotatedImage implements Image{

	private String fileName;
	
	 public RotatedImage(String fileName) {
		super();
		this.fileName = fileName;
	}

	public void display() {
	      System.out.println("Display rotated" + fileName);
	   }
}
