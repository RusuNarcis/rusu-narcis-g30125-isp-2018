package g30125.rusu.narcis.e1.l5;

public class Rectangle extends Shape {
protected double width;
protected double length;
public Rectangle()
{
	width=41;
	length=54;
}
public Rectangle(double width,double length)
{
	width=4;
	length=5;
}
public Rectangle(double width,double length,String color,boolean filled)
{
	width=2;
	length=1;
	color="recta";
	filled=false;
}
public double getWidth() {
	return width;
}
public void setWidth(double width) {
	this.width = width;
}
public double getLength() {
	return length;
}
public void setLength(double length) {
	this.length = length;
}
public double getArea()
{
	return width*length;
}
public double getPerimeter()
{
	return 2*(width+length);
}
@Override
public String toString() {
	return "Rectangle [width=" + width + ", length=" + length + "]";
}

}
