package rusu.narcis.g30125.ex3.l8;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class EncryptDecrypt {
	
	public void enc(String st)throws IOException{
		
		BufferedReader br = new BufferedReader(new FileReader(st));
		String s = new String();
		String s2 = new String();
		
		while((s=br.readLine())!=null) {
			s2+=s+"\n";
		}
		br.close();
	
	
	DataOutputStream out = null;
	
	try {
		out= new DataOutputStream(new FileOutputStream("exemplu.enc"));
		
	
		for(char ch:s2.toCharArray()) {
			ch++;
			out.writeChar(ch);
		}
		out.close();
	}catch(FileNotFoundException e) {
		System.out.println("File not found"+e);
	}
}	
	
	public void dec(String st)throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(st));
		String s = new String();
		String s2 = new String();
		
		while((s=br.readLine())!=null) {
			s2+=s+"\n";
		}
		br.close();
	
	
	DataOutputStream out = null;
	
	try {
		out= new DataOutputStream(new FileOutputStream("exemplu.dec"));
		
	
		for(char ch:s2.toCharArray()) {
			ch--;
			out.writeChar(ch);
		}
		out.close();
	}catch(FileNotFoundException e) {
		System.out.println("File not found"+e);
	}
		
	}

public static void main(String[] args) throws IOException {
	EncryptDecrypt ed = new EncryptDecrypt();
	
	System.out.println(">>fisierul>>:");
	Scanner sc = new Scanner(System.in);
	String st = sc.nextLine();
	int ans=0;
	
		
	System.out.println("Meniu:");
	System.out.println("false se cripteaza");
	System.out.println("true se decripteaza");
	System.out.println("a= ");
	Scanner scanner = new Scanner(System.in);
	//Scanner in = new Scanner(System.in);
	//ans = in.nextInt();
	boolean a = scanner.nextBoolean();
	scanner.close();
	sc.close();
	//in.close();
	
	if(a==false){
	ed.enc(st);
	} else {
		ed.dec(st);
		}
}
}
