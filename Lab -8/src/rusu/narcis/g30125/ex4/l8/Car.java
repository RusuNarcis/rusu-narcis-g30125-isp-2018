package rusu.narcis.g30125.ex4.l8;
import java.io.Serializable;

public class Car implements Serializable{
	
	private String model;
	private double price;
	public Car(String model,double price) {
		this.model=model;
		this.price=price;
	}
	public String getModel() {
		return model;
	}
	public double getPrice() {
		return price;
	}
	@Override
	public String toString() {
		return "Model=" + getModel() + ", pret=" + getPrice() + "]";
	}
	
}
