package rusu.narcis.g30125.ex4.l8;

public class Main {
	
	
	public static void main(String[] args) throws Exception{
		CarManager cm=new CarManager();
		Car car1=cm.createCar("Ford", 410);
		Car car2=cm.createCar("Ferrari", 120);
		Car car3 = cm.createCar("Cadilac", 780);
				
				
		cm.addCar(car1, "car1.txt");
		cm.addCar(car2, "car2.txt");
		cm.addCar(car3, "car3.txt");
		cm.removeCar("car1.txt");
		System.out.println(car1.toString());
		

	}
}

