package robot;
import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class r4
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Wall blockAve0 = new Wall(ny, 1, 1, Direction.NORTH);
      Wall blockAve1 = new Wall(ny, 1, 2, Direction.NORTH);
      Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);
      Wall blockAve3 = new Wall(ny, 2, 2, Direction.NORTH);
      Wall blockAve4 = new Wall(ny, 2, 1, Direction.SOUTH);
      Wall blockAve6 = new Wall(ny, 1,1, Direction.WEST);
      Wall blockAve7 = new Wall(ny, 2, 1, Direction.WEST);
      Robot mark = new Robot(ny, 1, 2, Direction.SOUTH);
      Thing parcel = new Thing(ny, 2, 2);
 
      // mark goes around the roadblock
     
      
      mark.turnLeft();
      mark.turnLeft();
      mark.turnLeft();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.turnLeft();
     
      mark.move();
      mark.pickThing();
      mark.turnLeft();
      mark.turnLeft();
    
      mark.move();
      mark.turnLeft();
      mark.turnLeft();
      mark.turnLeft();
      mark.move();
      mark.turnLeft();
      mark.turnLeft();
      mark.turnLeft();
      mark.move();
      mark.turnLeft(); mark.turnLeft(); mark.turnLeft();
      
      
   }
} 