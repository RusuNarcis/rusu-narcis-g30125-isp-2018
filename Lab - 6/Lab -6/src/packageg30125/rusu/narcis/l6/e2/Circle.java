package packageg30125.rusu.narcis.l6.e2;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape{

    private int radius;
	private Color color;
	private String id;
	private int x;
	private int y;
	private boolean fill;
	
    public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	@Override
	public String getId() {
		return id;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public boolean isFill() {
		return fill;
	}
	public Circle(Color color, String id, int x, int y, int radius,boolean fill) {
    	
        this.radius = radius;
        this.color=color;
        this.id=id;
        this.x=x;
        this.y=y;
        this.fill=fill;
	}
    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==false) {
        g.drawOval(getX(),getY(),radius,radius);}
        else g.fillOval(getX(), getY(),radius,radius) ;
        
   
    }
}