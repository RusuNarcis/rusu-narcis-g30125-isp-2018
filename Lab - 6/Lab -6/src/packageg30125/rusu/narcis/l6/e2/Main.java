package packageg30125.rusu.narcis.l6.e2;

import java.awt.Color;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s3 = new Rectangle(Color.BLACK,"R1",43,67, 200,true);
        b1.addShape(s3);
        Shape s1 = new Circle(Color.RED,"C1",50,74,90,true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN,"C2",78,80,100,false);
        b1.addShape(s2);
       
        //b1.deleteById("C1");

    }
}
