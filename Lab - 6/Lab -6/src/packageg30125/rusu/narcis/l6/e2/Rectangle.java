package packageg30125.rusu.narcis.l6.e2;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape{
	private Color color;
	private String id;
	private int x;
	private int y;
	private boolean fill;
	private int length;
    
    public Rectangle(Color color, String id, int x, int y, int length, boolean fill){
        this.color=color;
        this.id=id;
        this.x=x;
        this.y=y;
        this.fill=fill;
        this.length = length;
    }

    public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public String getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isFill() {
		return fill;
	}

	public int getLength() {
		return length;
	}
	
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==false) {
        g.drawRect(getX(),getY(),length,length);}
        else g.fillRect(getX(), getY(), length, length);
       
    }
}
