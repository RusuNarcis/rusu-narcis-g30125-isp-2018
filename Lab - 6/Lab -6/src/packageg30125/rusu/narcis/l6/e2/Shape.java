package packageg30125.rusu.narcis.l6.e2;

import java.awt.Graphics;

public interface Shape {
	
    public abstract void draw(Graphics g);
    String getId();

	/*public void deleteById() {
	}/*/
}
