package packageg30125.rusu.narcis.l6.e5;

import java.awt.Color;



public class Pyramid extends Rectangle{
	
	public Pyramid(Color color, int x, int y, int length, int width, String id, boolean fill) {
		super(color, x, y, length, width, id, fill);
		// TODO Auto-generated constructor stub
	}
	private static final int nr = 8;
	private static final int width = 15;
	private static final int height = 30;
	
	public static void createPyramid() {
		
		DrawingBoard b1 = new DrawingBoard();
		int i,j,c=0;
		
		for(i=0;i<=nr;i++) {			
			for(j=i;j>0;j--) {
				int x = height*(j+1)+(nr-i)*height/2;
				int y = 500-width*(nr-i+1);
				Shape brick = new Rectangle(Color.BLACK,x,y,height,width,"c",false);
				b1.addShape(brick);
				c++;
				
			}
			
		}
	}
	public static void main(String[] args) {
		createPyramid();
	}
}