package packageg30125.rusu.narcis.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends Shape{

    private static int length,width;

    public Rectangle(Color color,int x,int y, int length,int width,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
        this.width=width;
        
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(getFill()==false)
        g.drawRect(getx(),gety(),length,width);
        else g.fillRect(getx(), gety(), length, width);
    }

	public static int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public static int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
    
   
}