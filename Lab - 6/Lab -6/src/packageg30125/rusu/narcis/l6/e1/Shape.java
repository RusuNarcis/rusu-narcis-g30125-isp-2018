package packageg30125.rusu.narcis.l6.e1;
import java.awt.Color;
import java.awt.Graphics;

public abstract class Shape {
	private Color color;
	private String id;
	private int x;
	private int y;
	private boolean fill;

    public boolean isFill() {
		return fill;
	}
	public Shape(Color color, String id, int x, int y, boolean fill) {
        this.color = color;
        this.id=id;
    	this.x = x;
    	this.y = y;
    	this.fill=fill;
    }
    public Color getColor() {
        return color;
    }
    public String getId() {
  		return id;
  	} 
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);

	/*public void deleteById() {
	}/*/
}
