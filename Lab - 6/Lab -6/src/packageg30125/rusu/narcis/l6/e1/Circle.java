package packageg30125.rusu.narcis.l6.e1;

import java.awt.Color;
import java.awt.Graphics;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, String id, int x, int y, int radius,boolean fill) {
    	  super(color,id,x,y,fill);
        this.radius = radius;
    }
    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill()==false) {
        g.drawOval(getX(),getY(),radius,radius);}
        else g.fillOval(getX(), getY(),radius,radius) ;
        
   
    }
}