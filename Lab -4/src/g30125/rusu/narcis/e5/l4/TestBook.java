package g30125.rusu.narcis.e5.l4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30125.rusu.narcis.e4.l4.Author;

public class TestBook {

	@Test
	public void testToString() {
		Author a1 = new Author("a2","a2@yahoo.com",'f');
		Book b1 = new Book("b1",a1,100.02,34);
		assertEquals("book name b1 by Author a2 ( f ) at email a2@yahoo.com",b1.toString());
	}
	
	@Test
	public void testGet() {
		Author a1 = new Author("a2","a2@yahoo.com",'f');
		Book b1 = new Book("b1",a1,100.02,34);
		assertEquals("b1",b1.getName());
	}
}
