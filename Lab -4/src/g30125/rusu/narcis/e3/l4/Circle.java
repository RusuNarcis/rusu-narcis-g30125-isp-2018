package g30125.rusu.narcis.e3.l4;

public class Circle {

	private double radius;
	private String color;
	
	public Circle()
	{
		radius = 1.0;
		color ="red";
	}
	
	public Circle(double radius)
	{
		this.radius=radius;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public double getArea() {
		return Math.PI*(this.radius)*(this.radius);
	}
}
