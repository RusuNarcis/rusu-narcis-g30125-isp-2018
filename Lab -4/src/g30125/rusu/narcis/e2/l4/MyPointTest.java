package g30125.rusu.narcis.e2.l4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MyPointTest {

	@Test
	public void testSetXY() {
		MyPoint point = new MyPoint(8, 2);

		point.setXY(10, 10);

		assertEquals(10, point.getX());
		assertEquals(10, point.getY());
	}

	@Test
	public void testToString() {
		MyPoint point = new MyPoint(8, 2);
		point.toString();

		assertEquals("(8, 2)", point.toString());
	}

	@Test
	public void testDistanceWithOneParameter() {
		MyPoint point1 = new MyPoint(5, 7);
		MyPoint point2 = new MyPoint(1, 2);

		assertEquals(6.4031, point1.distance(point2), 0.0001);
	}

	@Test
	public void testDistanceWithTwoParameters() {
		MyPoint point1 = new MyPoint(5, 7);

		assertEquals(6.4031, point1.distance(1, 2), 0.0001);
	}
}
